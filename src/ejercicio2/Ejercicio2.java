/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

/**
 *
 * @author duacos
 */
public class Ejercicio2 {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        
        Piano piano = new Piano();
        Guitarra guitarra = new Guitarra();
        Trompeta trompeta = new Trompeta();
        Bajo bajo = new Bajo();
        
        System.out.println("------------------------------");
        System.out.println("pareparación y afinación");
        System.out.println("------------------------------");
        piano.afinar();
        guitarra.afinar();
        trompeta.afinar();
        bajo.afinar();
        System.out.println("------------------------------");
        System.out.println("empieza el concierto");
        System.out.println("------------------------------");
        piano.tocar();
        guitarra.tocar();
        trompeta.tocar();
        bajo.tocar();
        
    }
    
}
