/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio2;

/**
 *
 * @author duacos
 */
public class Piano extends Instrumento {
    
    public void tocar() {
        System.out.println("Sonido de piano");
    }
    
    public void afinar() {
        System.out.println("Afinación de piano");
    }
}
