
package ejercicio2;

/**
 *
 * @author duacos
 */
public class Trompeta extends Instrumento {
    
    public void tocar() {
        System.out.println("Sonido de trompeta");
    }
    
    public void afinar() {
        System.out.println("afinación de trompeta");
    }
}
