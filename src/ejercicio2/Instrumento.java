
package ejercicio2;


public class Instrumento {
    
    
    public void tocar() {
        System.out.println("Sonido de instrumento");
    }
    
    public void afinar() {
        System.out.println("Afinación de instrumento");
    }
    
}
